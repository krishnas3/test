module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  env: {
    production: {
      plugins: ['transform-remove-console', 'react-native-reanimated/plugin'], //removing console.log from app during release (production) versions
    },
  },
  plugins: ['react-native-reanimated/plugin'],
};
