// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, { useState, useEffect } from 'react';
import {
    ActivityIndicator,
    View,
    StyleSheet,
    Image, Text, Alert, ToastAndroid
} from 'react-native';
import { profile, screen } from '../assets/Index';


const SplashScreen = ({ navigation }) => {
    //State for ActivityIndicator animation
    const [animating, setAnimating] = useState(true);
    const [loading, setLoading] = React.useState(false);
 


    useEffect(() => {
        setTimeout(() => {
            navigation.navigate(
                'Dashboard'
            )
        }, 4000);
       
    }, []);

   
    return (
        <View style={styles.container}>
            <Image
                source={profile}
                style={{ width: 50, resizeMode: 'contain', margin: 30, height: 50 }}
            />
           

            {/* <ActivityIndicator
        animating={animating}
        color="#FFFFFF"
        size="large"
        style={styles.activityIndicator}
      /> */}
        </View>
    );
};

export default SplashScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    activityIndicator: {
        alignItems: 'center',
        height: 80,
    },
});