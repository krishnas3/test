import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { cardImage, details } from '../assets/Index';
import { Rating } from 'react-native-ratings';
import { ScrollView } from 'react-native-gesture-handler';


const Details = () => {
  const formatMRP = (mrp) => {
    return mrp.replace(/\$/, '₹') // Replace $ with ₹
              .replace(/^₹(\d+)$/, (match, p1) => `₹${Number(p1).toFixed(3)}`); // Format MRP as a 5-digit number
  };
  const dataList = [
    { date: 'January 1, 2023', tax: '10%', rate: '20', mrp: '$2415' },
    { date: 'January 1, 2023', tax: '8%', rate: '2518', mrp: '$2552' },
    { date: 'January 1, 2023', tax: '10%', rate: '120', mrp: '$125' },
    { date: 'January 1, 2023', tax: '8%', rate: '3518', mrp: '$20452' },
    { date: 'January 1, 2023', tax: '10%', rate: '2220', mrp: '$2552' },
    { date: 'January 2, 2023', tax: '8%', rate: '7718', mrp: '$24522' },
    { date: 'January 1, 2023', tax: '10%', rate: '4420', mrp: '$225' },
    { date: 'January 2, 2023', tax: '8%', rate: '4418', mrp: '$2252' },
  ];
  const groupBy = (array, key) => {
    return array.reduce((result, item) => {
      (result[item[key]] = result[item[key]] || []).push(item);
      return result;
    }, {});
  };
  const groupedData = groupBy(dataList, 'date');
  return (
    <>
      <ScrollView>
        <View style={styles.parentContainer}>
          <View style={styles.childContainer1}>
            <Text style={{}}>Booking ID :    <Text style={{ color: "#464646" }}>84596857893</Text></Text>
          </View>
          <View style={styles.childContainer2}>
            <Image source={details} style={styles.image} />
            <View style={styles.textContainer}>
              <Text style={styles.title}>Service Title</Text>
              <Text style={styles.subtitle}>Lorem ipsum dolor sit amet consectetur. At viverra pharetra.</Text>
            </View>
            <View style={styles.ratingContainer}>
              <Text style={{ bottom: 2 }}>5.0 | </Text>
              <Rating
                type="star"
                ratingCount={5}
                startingValue={4} // Set the initial rating as needed
                imageSize={15}
                onFinishRating={(rating) => console.log('Rating:', rating)}

              />


            </View>
          </View>
       
            {Object.keys(groupedData).map((date, index) => (
              <View key={index} style={styles.childContainer3}>
                <View style={{}}>
                <Text style={styles.date}>{date}</Text>
              </View>
                {groupedData[date].map((data, dataIndex) => (
                  <View key={dataIndex} style={styles.listItem}>
                    <View style={{flexDirection:'row',alignItems:'center',margin:10}}>
                    <Text style={styles.tax}>-{data.tax} </Text>
                    <Text style={styles.rate}>{data.rate} </Text>
                    <Text style={styles.subtitle}>Inclusive of all taxes.</Text>
                    </View>
                  
                    <Text style={{fontSize:14,color:"#464646",margin:10}}>M.R.P : <Text style={[styles.mrp,{ textDecorationLine: 'line-through' }]}>{formatMRP(data.mrp)}</Text></Text>
                  </View>
                ))}
              </View>
            ))}
          </View>
          
       
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  parentContainer: {
    flex: 1,


    padding: 5,
    margin: 5,
    borderColor: "#CACACA",
    borderWidth: 1,
    borderRadius: 8,
  },
  childContainer1: {

    padding: 10,
    margin: 5,
    borderColor: "#CACACA",
    borderWidth: 1,
    borderRadius: 8,
  },
  childContainer2: {
    flexDirection: 'row',
    backgroundColor: '#CACACA',
    padding: 10,
    margin: 5,

    borderRadius: 8,
    // alignItems: 'center',
  },
  image: {
    width: 68,
    height: 89,

    marginRight: 10,
  },
  textContainer: {
    flex: 1,
  },
  title: {
    color: '#464646',
    fontSize: 20,
    fontWeight: 'bold',
  },
  subtitle: {
    color: '#A2A2A3',
    fontSize: 14,
  },
  ratingContainer: {
    flexDirection: 'row',

    width: 100, height: 15,
    // marginLeft: 10,
    alignItems: "center"
  },
  textOnRight: {
    color: 'white',
    // marginLeft: 10,
    width: 100
  },
  childContainer3: {

  },
  childContainer4: {
    backgroundColor: 'yellow',
    padding: 10,
    margin: 5,
    borderRadius: 8,
  },
  listItem: {
    marginBottom: 10,
    top: 20,
 
    borderColor: "#CACACA",
    borderWidth: 1,
    borderRadius: 8,
    padding: 10,
  },
  date: {
    color: 'white',
    bottom:10,
    fontSize: 14,
    top: 10,
    paddingLeft: 10,
    padding: 5,
    // fontWeight: 'bold',
    borderColor: "#CACACA",
    borderWidth: 1,
    borderRadius: 8,
    backgroundColor: '#4C7CA9'
  },
  tax: {
    color: '#D64D49',
    fontSize: 20,
  },
  rate: {
    color: '#464646',
    fontSize: 25,
    fontWeight:"500"
  },
  mrp: {
    color: '#A2A2A3',
    fontSize: 14,
    
  },
  text: {
    // color: 'white',
    // fontWeight: 'bold',
  },
});

export default Details;
