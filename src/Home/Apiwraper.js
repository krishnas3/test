import axios from "axios";
import { BASE_URL } from "../assets/Index";

// export const getBookingDetailsRequest = async (data) => {
//   const res = await axios({
//     method: 'GET',
//     url: BASE_URL + '/getBookingDetails/'+ data,
//   });
//   return res;
// };

export const getApi = async ({headers}) => {
  const res = await axios({
    method: 'GET',
    headers: headers,
    url:BASE_URL +'api/colors/new?format=json',
  });
  return res;
};

// export const PostMethodBookingRequest = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/getDayDetails/',
//     data: data,
//   });
//   return res;
// };

// export const PostMethodRequest = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/getParticularSlotBooking',
//     data: data,
//   });
//   return res;
// };

// export const SlotSpaceRequest = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/editSlotStatusToMoreBooking',
//     data: data,
//   });
//   return res;
// };

// export const UserDetailsRequest = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/getEmployeeDetails',
//     data: data,
//   });
//   return res;
// };

// export const EidtSlotBookingRequest = async (data) => {
//   const res = await axios({
//     method: 'POST',
//     headers: {
//       'Content-Type': 'application/json'
//     },
//     url: BASE_URL + '/editBookingStatus',
//     data: data,
//   });
//   return res;
// };
// export const EidtSlotStatusRequest = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/editSlotStatus',
//     data: data,
//   });
//   return res;
// };
// export const PayementRequest = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/getAllPayments',
//     data: data,
//   });
//   return res;
// };

// export const FCMToken = async (data,{headers}) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL +'/registerToken',
//     data: data,
//   });
//   return res;
// };


// export const EidtSlotStatusAllRequest = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/updateAllSlotsOfday/',
//     data: data,
//   });
//   return res;
// };

// export const DeletePayementRequest = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/removePaymentId',
//     data: data,
//   });
//   return res;
// };

// export const SendSMSInvoice = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/SMSInvoice',
//     data: data,
//   });
//   return res;
// };


// export const AddPayment = async (data, { headers }) => {
//   const res = await axios({
//     method: 'POST',
//     headers: headers,
//     url: BASE_URL + '/addPayment',
//     data: data,
//   });
//   return res;
// };

