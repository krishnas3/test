import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { cancel, cardImage, details } from '../assets/Index';
import { Rating } from 'react-native-ratings';
import { ScrollView } from 'react-native-gesture-handler';
import { Checkbox, Button } from 'react-native-paper';

const Paymenet = ({ closePaperModal, openModal }) => {
    const [selectedEntries, setSelectedEntries] = React.useState([]);
    const handlePressPaper = () => {
        if (openModal) {
            openModal([]);
        }
    };
    const handlePressYes = () => {
        if (openModal) {
            openModal(selectedEntries);
        }
    };




    const handleCheckboxChange = (date, type) => {
        const isSelected = selectedEntries.some((entry) => entry.date === date && entry.type === type);

        if (isSelected) {
            // If already selected, remove from the list
            setSelectedEntries((prevSelectedEntries) =>
                prevSelectedEntries.filter((entry) => !(entry.date === date && entry.type === type))
            );
        } else {
            // If not selected, add to the list
            setSelectedEntries((prevSelectedEntries) => [...prevSelectedEntries, { date, type }]);
        }
    };
    const handleViewDetailsPressPaper = () => {
        if (closePaperModal) {
            closePaperModal(selectedEntries);
        }
    };

    const organizedData = {
        "January 1, 2023": {
            "A": [{ detailB: 'BBAAAB B', detailA: 'AAAABB A', delivery: true, amount: 100, active: true, detailsActive: true }],
            "B": [{ detailA: 'AA AAAAA', detailB: 'BBBVVF B', delivery: false, amount: 150, active: true, detailsActive: true }],
        },
        "January 2, 2023": {
            "A": [{ detailB: 'BBBAAAA B', detailA: 'AA DDDA', delivery: true, amount: 120, active: false, detailsActive: true }],
            "B": [{ detailB: 'BBBSSS B', detailA: 'AAAAA A', delivery: true, amount: 200, active: true, detailsActive: false }],
        },
    };



    return (
        <>


            <View style={styles.parentContainer}>
                <View style={styles.container}>
                    {/* Centered Text */}
                    <View style={styles.textContainer}>
                        <Text style={styles.bookingText}>
                            Confirmation
                        </Text>
                    </View>

                    {/* Image in the right corner */}
                    <TouchableOpacity style={styles.imageContainer} onPress={handleViewDetailsPressPaper}>
                        <Image style={styles.image} source={cancel} />
                    </TouchableOpacity>
                </View>
                <ScrollView style={{ marginBottom: 55 }}>
                    <View
                        style={{
                            backgroundColor: 'white',
                            height: 50,
                            padding: 10,
                            top: 2,
                            alignItems: 'center',
                            flexDirection: 'row',
                        }}  >

                        <Text style={{ color: '#818181', }}>Payment type : <Text style={{ color: '#434343', fontSize: 14 }}>Online</Text></Text>
                    </View>

                    <Text style={{ color: '#818181', backgroundColor: 'white', top: 5, margin: 5 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text>

                    <View style={{ top: 10, marginBottom: 30 }}>
                        {Object.keys(organizedData).map((date, dateIndex) => (
                            <View key={dateIndex}>
                                <Text style={styles.dateText}>{date}</Text>
                                <View
                                    style={[
                                        styles.dateContainer,
                                        { margin: 5, backgroundColor: dateIndex % 2 === 0 ? 'white' : 'white', borderWidth: 1, borderRadius: 10, borderColor: "#CACACA" },
                                    ]}
                                >
                                    {Object.keys(organizedData[date]).map((type, typeIndex) => (
                                        <View key={typeIndex}>
                                            <Text style={styles.typeText}>Type: {type}</Text>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View
                                                    style={[
                                                        styles.entryContainer,
                                                        {
                                                            backgroundColor: organizedData[date][type][0].active ? 'white' : '#CACACA',
                                                        },
                                                    ]}
                                                >
                                                    <Text style={{ color: organizedData[date][type][0].active ? '#818181' : '#EDEDED' }}>
                                                        Amount to be paid
                                                    </Text>
                                                    {organizedData[date][type].map((entry, entryIndex) => (
                                                        <View
                                                            key={entryIndex}
                                                            style={{
                                                                flexDirection: 'row',
                                                                margin: 5,
                                                                padding: 10,
                                                                borderRadius: 10,
                                                            }}
                                                        >
                                                            <View style={{}}>
                                                                <Text style={{ color: entry.active ? '#434343' : '#EDEDED', fontSize: 18 }}>
                                                                    ₹ {entry.amount}/-
                                                                    <Text style={{ color: entry.active ? '#818181' : '#EDEDED', fontSize: 12 }}>
                                                                        Inclusive all taxes
                                                                    </Text>
                                                                </Text>
                                                                <Text style={{ color: entry.active ? '#818181' : '#EDEDED', fontSize: 14 }}>
                                                                    {entry.delivery && "+ Delivery charges"}
                                                                </Text>
                                                            </View>
                                                            <View style={{}}>
                                                                <Text style={{ color: entry.active ? '#818181' : '#EDEDED', fontSize: 12 }}>detail A</Text>
                                                                <Text
                                                                    style={{
                                                                        color: entry.active ? (entry.detailsActive ? '#EDA136' : '#EDA136') : '#EDEDED',
                                                                        fontSize: 14,
                                                                    }}
                                                                >
                                                                    {entry.detailA}
                                                                </Text>
                                                                <Text style={{ color: entry.active ? '#818181' : '#EDEDED', fontSize: 12 }}>detail B</Text>
                                                                <Text
                                                                    style={{
                                                                        color: entry.active ? (entry.detailsActive ? '#EDA136' : 'red') : '#EDEDED',
                                                                        fontSize: 14,
                                                                    }}
                                                                >
                                                                    {entry.detailB}
                                                                </Text>
                                                            </View>
                                                        </View>
                                                    ))}
                                                </View>
                                                <View style={{ width: 30, justifyContent: 'center' }}>
                                                    <Checkbox
                                                        status={selectedEntries.some((entry) => entry.date === date && entry.type === type) ? 'checked' : 'unchecked'}
                                                        color="#4C7CA9"
                                                        style={styles.checkbox}
                                                        onPress={() => handleCheckboxChange(date, type)}
                                                    />
                                                </View>
                                            </View>
                                        </View>
                                    ))}
                                </View>
                            </View>
                        ))}
                    </View>
                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={{ width: 100, borderColor: '#4C7CA9', backgroundColor: 'white', height: 30, borderRadius: 5, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} onPress={handlePressPaper}>
                            <Text style={{ color: '#4C7CA9', fontSize: 16, }}>No</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: 100, borderColor: '#4C7CA9', backgroundColor: '#4C7CA9', height: 30, borderRadius: 5, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} onPress={handlePressYes}>
                            <Text style={styles.buttonText}>Yes</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>



        </>
    );
};

const styles = StyleSheet.create({
    parentContainer: {
        backgroundColor: 'white'



    },
    childContainer1: {
        height: 47,
        padding: 10,
        backgroundColor: "#4C7CA9",
        borderColor: "#4C7CA9",

    },
    childContainer2: {
        flexDirection: 'row',
        backgroundColor: '#CACACA',
        padding: 10,
        margin: 5,

        borderRadius: 8,
        // alignItems: 'center',
    },
    image: {
        width: 68,
        height: 89,

        marginRight: 10,
    },
    textContainer: {
        flex: 1,
    },
    title: {
        color: '#464646',
        fontSize: 20,
        fontWeight: 'bold',
    },
    subtitle: {
        color: '#A2A2A3',
        fontSize: 14,
    },
    ratingContainer: {
        flexDirection: 'row',

        width: 100, height: 15,
        // marginLeft: 10,
        alignItems: "center"
    },
    textOnRight: {
        color: 'white',
        // marginLeft: 10,
        width: 100
    },
    childContainer3: {

    },
    childContainer4: {
        backgroundColor: 'yellow',
        padding: 10,
        margin: 5,
        borderRadius: 8,
    },
    listItem: {
        marginBottom: 10,
        top: 20,

        borderColor: "#CACACA",
        borderWidth: 1,
        borderRadius: 8,
        padding: 10,
    },
    date: {
        color: 'white',
        bottom: 10,
        fontSize: 14,
        top: 10,
        paddingLeft: 10,
        padding: 5,
        // fontWeight: 'bold',
        borderColor: "#CACACA",
        borderWidth: 1,
        borderRadius: 8,
        backgroundColor: '#4C7CA9'
    },
    tax: {
        color: '#D64D49',
        fontSize: 20,
    },
    rate: {
        color: '#464646',
        fontSize: 25,
        fontWeight: "500"
    },
    mrp: {
        color: '#A2A2A3',
        fontSize: 14,

    },
    text: {
        // color: 'white',
        // fontWeight: 'bold',
    },



    buttonContainer: {
        flexDirection: 'row',
        justifyContent: "space-around",
        padding: 10,
        bottom: 20
    },
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 10,
        backgroundColor: '#4C7CA9'
    },
    textContainer: {
        flex: 1,
        alignItems: 'center',
    },
    bookingText: {
        textAlign: 'center',
        color: 'white',
        fontSize: 22
    },
    imageContainer: {
        marginLeft: 10,
    },
    image: {
        width: 15,
        height: 15,
        resizeMode: 'contain',
    },
    dateContainer: {
        padding: 10,
        marginBottom: 10,
    },
    dateText: {
        fontWeight: 'bold',
        // backgroundColor: '#4C7CA9',
        // padding: 5, borderColor: '#CACACA',
        // borderWidth: 1,
        borderRadius: 10, margin: 5,
        alignSelf: 'center',
        color: '#434343'
    },
    entryContainer: {
        borderColor: '#CACACA',
        borderWidth: 1,
        borderRadius: 10,
        padding: 10,
        marginVertical: 10,
    },
    detailText: {
        fontSize: 14,
        fontWeight: 'bold',
        marginVertical: 5,
    },
    entryText: {
        fontSize: 14,
        color: '#464646',
        marginVertical: 5,
    },
    checkbox: {
        // alignSelf:'center'
    },
    buttonText: {
        color: 'white',
        fontSize: 16,
    },
});

export default Paymenet;
