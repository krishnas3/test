

import { View, Text, SafeAreaView, TouchableOpacity, StyleSheet } from 'react-native'
import React from 'react'
import { AngleDown, Calllog, arrow, back, calendar, filter, menu, option1, option2, option3, option4, option5, option6, profile, settings } from '../assets/Index'
import { Image } from 'react-native'
import { useState } from 'react'
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { Provider, Modal, Portal, Button } from 'react-native-paper';
import moment from 'moment';
import { Drawer } from 'react-native-drawer-layout';
import SwitchSelector from "react-native-switch-selector";
import BookingCard from './BookingCrad'
import Details from './Details'
import Paymenet from './Paymenet'
import { getApi } from './Apiwraper'

// import { Drawer } from 'react-native-drawer-layout';
const HomeScreen = ({ navigation }) => {

  const [selectedDate1, setSelectedDate1] = useState(new Date());
  const [date, setDate] = useState((moment(selectedDate1).format('YYYY-MM-DD')));
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [Option, setOption] = React.useState('Option 1');
  const [isPaperModalVisible, setPaperModalVisible] = useState(false);
  const [data, setData] = useState([])
  const [data1, setData1] = useState()
  const handleNoButtonClick = () => {
    // Set selected entries to an empty array
    // setSelectedEntries([]);
    // Close the confirmation modal
    closePaperModal();
  };
  const openPaperModal = () => {
    setPaperModalVisible(true);
  };

  const closePaperModal = () => {
    setPaperModalVisible(false);
  };

  const [ModalVisible, setModalVisible] = useState(false);

  const openModal = (data) => {
    setModalVisible(true);

    setData(data)
    console.log(data)
  };

  const closeModal = () => {
    setModalVisible(false);
    setPaperModalVisible(false);
    console.log(data)
  };
  const [type, setType] = useState('Type A')
  const typeMethod = (data) => {
    setType(data)
    setData([])
    console.log(data)
  };
  const switchoptions = [
    { label: "Type A", value: "Type A" },
    { label: "Type B", value: "Type B" },

  ];
  // method for show date modal
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
  // method for hide date modal
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
    console.log(date)

  };
  // method for confirm date modal
  const handleConfirm = (date) => {
    setDate((moment(date).format('YYYY-MM-DD')));

    hideDatePicker();
  };


  const handleOptionAClick = (optionText) => {
    // Handle the click event here, and you can use the optionText parameter
    console.log(`Clicked on Option A with text: ${optionText}`);
    setOpen(false)
    setOption(optionText)
    // You can perform any other actions with the optionText as needed
  };
  //   // header for api
  const headers = {
    'Content-Type': 'application/json',
  };


  const getUserData = async () => {
    await getApi(
      { headers: headers }).then(res => {
        setData1(res.data)
        // console.log(res.data, 'get uswer api')
      }).catch(error => {
        console.log(error)
      })

  }

  React.useEffect(() => {
    getUserData()
    console.log("Message")
  }, []);


  const renderDrawerContent = () => {
    return (
      <SafeAreaView style={{ flex: 1, alignItems: 'center' }}>
        <View style={{ backgroundColor: "#4C7CA9", width: 150, alignItems: 'center', height: 50, justifyContent: "center" }}>
          <TouchableOpacity
            style={{ backgroundColor: 'white', fontSize: 14, borderRadius: 50, padding: 5 }}
            onPress={() => handleOptionAClick("Option 3")}
          >
            <Image
              style={{ tintColor: "#4C7CA9", width: 25, height: 25, resizeMode: 'contain', borderRadius: 50, transform: [{ rotate: '180deg' }] }}
              source={arrow}
            />
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={styles.drawerOption} onPress={() => handleOptionAClick("Option 1")}>
          <Image
            style={{ width: 15, height: 15, resizeMode: 'contain' }}
            source={option1}
          />
          <Text style={styles.drawerText}>Option 1</Text>
        </TouchableOpacity>
        <View style={styles.divider} />
        <TouchableOpacity style={styles.drawerOption} onPress={() => handleOptionAClick("Option 2")}>
          <Image
            style={{ width: 15, height: 15, resizeMode: 'contain' }}
            source={option2}
          />
          <Text style={styles.drawerText}>Option 2</Text>
        </TouchableOpacity>
        <View style={styles.divider} />
        <TouchableOpacity style={styles.drawerOption} onPress={() => handleOptionAClick("Option 3")}>
          <Image
            style={{ width: 15, height: 15, resizeMode: 'contain' }}
            source={option3}
          />
          <Text style={styles.drawerText}>Option 3</Text>
        </TouchableOpacity>
        <View style={styles.divider} />
        <TouchableOpacity style={styles.drawerOption} onPress={() => handleOptionAClick("Option 4")}>
          <Image
            style={{ width: 15, height: 15, resizeMode: 'contain' }}
            source={option4}
          />
          <Text style={styles.drawerText}>Option 4</Text>
        </TouchableOpacity>
        <View style={styles.divider} />
        <TouchableOpacity style={styles.drawerOption} onPress={() => handleOptionAClick("Option 5")}>
          <Image
            style={{ width: 15, height: 15, resizeMode: 'contain' }}
            source={option5}
          />
          <Text style={styles.drawerText}>Option 5</Text>
        </TouchableOpacity>
        <View style={styles.divider} />
        <TouchableOpacity style={styles.drawerOption} onPress={() => handleOptionAClick("Option 6")}>
          <Image
            style={{ width: 15, height: 15, resizeMode: 'contain' }}
            source={option6}
          />
          <Text style={styles.drawerText}>Option 6</Text>
        </TouchableOpacity>
        <View style={styles.divider} />

      </SafeAreaView>
    );
  };
  return (
    <Provider>
      <Drawer
        open={open}
        onOpen={() => setOpen(true)}
        onClose={() => setOpen(false)}
        renderDrawerContent={renderDrawerContent}
        drawerStyle={{ top: 130, width: 150 }}
      >
        <SafeAreaView>

          <View
            style={{
              backgroundColor: 'white',
              height: 60,
              justifyContent: 'space-between',
              alignItems: 'center',
              elevation: 10,

              borderBottomEndRadius: 10,
              // Change this to the desired color for elevation/shadow
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.8,
              shadowRadius: 2,
              flexDirection: 'row',
            }}  >
            <Image
              style={{ margin: 5, width: 50, height: 50, resizeMode: 'contain' }}
              source={profile}
            />
            <TouchableOpacity
              style={{ flexDirection: 'row' }}
              onPress={() => {
                // Handle text press action here
              }}
            >
              <Text style={{ fontSize: 18, fontWeight: '100', color: '#434343' }}>
                Delhi, India
              </Text>
              <Image
                style={{ margin: 5, width: 20, height: 15, resizeMode: 'contain', tintColor: "#434343" }}
                source={AngleDown}
              />
            </TouchableOpacity>

          </View>
          <View
            style={{
              backgroundColor: "white",
              height: 60,
              top: 5,
              justifyContent: 'space-between',
              alignItems: 'center',
              elevation: 3,
              borderWidth: 2,
              borderColor: 'white',
              flexDirection: 'row',

              // Set flexDirection to 'row' to align the image and text horizontally
            }}
          >
            <View>
              <TouchableOpacity onPress={() => setOpen((prevOpen) => !prevOpen)}>
                <View style={{ flexDirection: 'row' }}>
                  <Image
                    style={{ margin: 5, width: 35, height: 35, resizeMode: 'contain' }}
                    source={menu}
                  />
                  <Text style={{ fontSize: 18, fontWeight: '100', color: '#434343', alignSelf: 'center' }} >
                    {Option}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', right: 5 }}>
              <View style={{ justifyContent: 'center', right: 15 }}>
                <TouchableOpacity

                  onPress={showDatePicker}
                >
                  <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                    <Image style={{ margin: 5, width: 20, height: 20, resizeMode: 'contain', tintColor: '#50AFE4' }} source={calendar} />
                  </View>
                  <Text style={{ color: '#50AFE4', fontSize: 15, }}>
                    Calendar
                  </Text>
                  <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="date"
                    onConfirm={handleConfirm}
                    onCancel={hideDatePicker}
                    style={{ backgroundColor: 'lightblue' }}
                  />
                </TouchableOpacity>
              </View>

              <View>
                <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
                  <Image style={{ margin: 5, width: 25, height: 20, resizeMode: 'contain', tintColor: '#50AFE4' }} source={filter} />
                </View>
                <Text style={{ color: '#50AFE4', fontSize: 15 }}>
                  filter
                </Text>

              </View>

            </View>
          </View>
          <View style={{ top: 10 }}>
            <SwitchSelector
              options={switchoptions}
              initial={0}
              onPress={(value) => typeMethod(value)}
              buttonColor="#FFFFFF"
              selectedColor="#4C7CA9"
              // borderColor="blue"
              textColor="#7B7B7B"
              backgroundColor="#CACACA"
              fontSize={14}
              hasPadding
            />
          </View>
          <View style={{ top: 10 }}>
            <BookingCard navigation={navigation} openPaperModal={openPaperModal} data={data} type={type} />
          </View>
          {/* React Native Paper Modal */}
          <Portal>
            <Modal visible={isPaperModalVisible} onDismiss={closePaperModal}>
              <View style={{ backgroundColor: "#CACACA", margin: 20, marginTop: 100 }}>
                <Paymenet closePaperModal={closePaperModal} closeModal={closeModal} openModal={openModal} handleNoButtonClick={handleNoButtonClick} />

                <Portal>
                  <Modal visible={ModalVisible} onDismiss={closeModal}>
                    <View style={styles.container}>
                      {/* Title View */}
                      <View style={styles.titleView}>
                        <Text style={styles.titleText}>Confirmation</Text>
                      </View>

                      {/* Content View */}
                      <View style={styles.contentView}>
                        <Text style={styles.contentText}>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </Text>
                      </View>

                      {/* Button View */}
                      <View style={styles.buttonView}>
                        <View style={styles.buttonContainer}>
                          <TouchableOpacity style={{ width: 100, borderColor: '#4C7CA9', backgroundColor: 'white', height: 30, borderRadius: 5, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} onPress={closeModal}>
                            <Text style={{ color: '#4C7CA9', fontSize: 16, }}>No</Text>
                          </TouchableOpacity>
                          <TouchableOpacity style={{ width: 100, borderColor: '#4C7CA9', backgroundColor: '#4C7CA9', height: 30, borderRadius: 5, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} onPress={closeModal}>
                            <Text style={styles.buttonText}>Yes</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </Modal>
                </Portal>
              </View>
            </Modal>
          </Portal>
          <TouchableOpacity style={{ margin: 50 }} onPress={() => navigation.navigate("ApiView", { data: data1 })}>
            <Text style={{ fontSize: 25, color: 'Blue' }}>Click here to show api </Text>
          </TouchableOpacity>
        </SafeAreaView>
      </Drawer>
    </Provider>
  )
}
const styles = StyleSheet.create({
  drawerOption: {
    alignItems: 'center',
    marginTop: 10,
  },
  drawerText: {
    color: 'black',
    fontSize: 14,
  },
  divider: {
    height: 1,
    backgroundColor: 'black',
    width: '80%', // Adjust the width of the divider as needed
    marginTop: 10,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: "900",
    textAlign: "center",
    padding: 20,
  },
  container: {
    backgroundColor: "white",
    margin: 20,
    // marginTop: 100,
    borderRadius: 10,
    padding: 15,
  },
  titleView: {
    // borderBottomWidth: 1,
    alignItems: 'center',
    // borderBottomColor: '#464646',
    paddingBottom: 10,
    marginBottom: 10,
  },
  titleText: {
    color: '#464646',
    fontSize: 24,
    fontWeight: '500',
  },
  contentView: {
    marginBottom: 20,
  },
  contentText: {
    color: '#818181',
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: "space-around",
    padding: 10,
    bottom: 20
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },


});

export default HomeScreen