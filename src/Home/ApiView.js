import { View, Text, ScrollView } from 'react-native'
import React from 'react'

const ApiView = ({ route}) => {
  return (
    <ScrollView>
    <View>
        {console.log(route.params.data,"kkkkkkkkk")}
      <Text>ApiView</Text>
      {route.params.data && Array.isArray(route.params.data) && route.params.data.length > 0 ? (
        <View>
          {route.params.data.map((item) => (
            <View key={item.id}>
              <Text>Title: {item.title}</Text>
              <Text>User: {item.userName}</Text>
              <Text>Views: {item.numViews}</Text>
              <Text>Votes: {item.numVotes}</Text>
              <Text>Comments: {item.numComments}</Text>
              {/* Add other fields you want to display */}
              <Text>-------------------------------</Text>
            </View>
          ))}
        </View>
      ) : (
        <Text>No data available</Text>
      )}
    </View>
    </ScrollView>
  )
}

export default ApiView