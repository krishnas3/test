import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { Checkbox, Button } from 'react-native-paper';
import { StyleSheet } from 'react-native';
import { cardImage, profile } from '../assets/Index';
import { Rating } from 'react-native-ratings';
import { useNavigation } from '@react-navigation/native';


const BookingCard = ({ navigation, openPaperModal, data, type }) => {


    const handleViewDetailsPressPaper = () => {
        if (openPaperModal) {
            openPaperModal();
        }
    };

    const handleViewDetailsPress = () => {

        navigation.navigate(
            'Details'
        )
    };

    return (
        <>
            {
                type == 'Type A' ? <View style={styles.cardContainer}>

                    <View style={[styles.cardHeader, { backgroundColor: '#4C7CA9', flexDirection: 'row', justifyContent: 'space-between', width: "100%" }]}>
                        <Text style={styles.headerText}>Date & Time : 12/12/1555</Text>
                        <Text style={styles.headerText}>Booking ID :215464614</Text>
                    </View>
                    <View style={styles.cardBody}>
                        <View style={styles.leftContent}>

                            <View>
                                <Text style={{ color: "black", fontWeight: "300" }}>Title : {type}</Text>
                                <Text style={{ fontSize: 12 }}>Lorem ipsum dolor sit amet, consectetur adipiscing</Text>
                            </View>
                            <View style={{ top: 5 }}>
                                <Text style={{ fontSize: 13 }}>Total Payable Amount :</Text>
                                <Text style={{ fontSize: 18, color: '#434343' }}>Rs.1999 /-</Text>
                                <Text style={{ fontSize: 12 }}>Inclusive of all taxes</Text>
                            </View>
                            <View style={{ top: 5 }}>
                                <Text style={{ fontSize: 12 }}>Location :</Text>

                                <Text style={{ fontSize: 13, color: "#434343" }}>Noida, 201301</Text>
                                {console.log(data, "kkk")}
                            </View>
                            {data.length === 0 ? <View style={{
                                borderWidth: 1,
                                borderColor: '#ddd',
                                elevation: 0.2,
                                margin: 10,
                                borderRadius:2,
                                shadowColor: 'black', // On iOS
                                shadowOffset: { width: 0, height: 10 },
                              
                              
                            }}>
                                <View style={styles.cardContent}>
                                    <Text style={styles.title}>Confirmation</Text>
                                    <Text style={styles.description}>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </Text>
                                    <TouchableOpacity style={styles.buttonContainers} onPress={handleViewDetailsPressPaper}>
                                        <Text style={styles.buttonText}>View Details</Text>
                                    </TouchableOpacity>
                                </View>
                            </View> : null
                            }

                            <View style={{ flexDirection: 'row', marginTop: 20 }}>

                                <Checkbox status={data && data.length > 0 ? 'checked' : 'unchecked'} color="#4C7CA9" style={styles.checkbox} />

                                {data === undefined || data.length === 0 ? (
                                    <Text style={styles.description}>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    </Text>
                                ) : (
                                    <Text style={[styles.description, { marginTop: 10 }]}>
                                        Detail A
                                    </Text>
                                )}
                            </View>
                        </View>
                        <View style={styles.rightContent}>
                            <TouchableOpacity onPress={handleViewDetailsPress}>
                                <Image source={cardImage} style={styles.image} />
                            </TouchableOpacity>

                            <View style={styles.ratingContainer}>
                                <Rating
                                    type="star"
                                    ratingCount={5}
                                    startingValue={4} // Set the initial rating as needed
                                    imageSize={10}
                                    onFinishRating={(rating) => console.log('Rating:', rating)}
                                    style={{ backgroundColor: 'black' }}
                                />
                                <Text style={styles.reviewText}>5 | </Text>
                                <Text style={styles.reviewText}>50 Reviews</Text>
                            </View>
                            <View style={styles.ratingContainer}>

                                <Text style={styles.reviewText}>Payment : </Text>
                                <Text style={{ color: "black" }}>Online</Text>
                            </View>
                        </View>

                    </View>

                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={{ width: 100, borderColor: '#4C7CA9', backgroundColor: 'white', height: 30, borderRadius: 5, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ color: '#4C7CA9', fontSize: 16, }}>No</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: 100, borderColor: '#4C7CA9', backgroundColor: '#4C7CA9', height: 30, borderRadius: 5, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{
                                fontSize: 12,
                                color: 'white'
                            }}>Yes</Text>
                        </TouchableOpacity>
                    </View>
                </View> : <View style={styles.cardContainer}>

                    <View style={[styles.cardHeader, { backgroundColor: '#4C7CA9', flexDirection: 'row', justifyContent: 'space-between', width: "100%" }]}>
                        <Text style={styles.headerText}>Date & Time : 12/12/1555</Text>
                        <Text style={styles.headerText}>Booking ID :215464614</Text>
                    </View>
                    <View style={styles.cardBody}>
                        <View style={styles.leftContent}>

                            <View>
                                <Text style={{ color: "black", fontWeight: "300" }}>Title : {type}</Text>
                                <Text style={{ fontSize: 12 }}>Lorem ipsum dolor sit amet, consectetur adipiscing</Text>
                            </View>
                            <View style={{ top: 5 }}>
                                <Text style={{ fontSize: 13 }}>Total Payable Amount :</Text>
                                <Text style={{ fontSize: 18, color: '#434343' }}>Rs.1999 /-</Text>
                                <Text style={{ fontSize: 12 }}>Inclusive of all taxes</Text>
                            </View>
                            <View style={{ top: 5 }}>
                                <Text style={{ fontSize: 12 }}>Location :</Text>

                                <Text style={{ fontSize: 13, color: "#434343" }}>Noida, 201301</Text>
                                {console.log(data, "kkk")}
                            </View>


                            <View style={{ flexDirection: 'row', marginTop: 20 }}>

                                <Checkbox status={data && data.length > 0 ? 'checked' : 'unchecked'} color="#4C7CA9" style={styles.checkbox} />

                                {data === undefined || data.length === 0 ? (
                                    <Text style={[styles.description, { marginTop: 10 }]}>
                                        {type}
                                    </Text>
                                ) : (
                                    <Text style={[styles.description, { marginTop: 10 }]}>
                                        Detail A
                                    </Text>
                                )}
                            </View>
                        </View>
                        <View style={styles.rightContent}>
                            <TouchableOpacity onPress={handleViewDetailsPress}>
                                <Image source={cardImage} style={styles.image} />
                            </TouchableOpacity>

                            <View style={styles.ratingContainer}>
                                <Rating
                                    type="star"
                                    ratingCount={5}
                                    startingValue={4} // Set the initial rating as needed
                                    imageSize={10}
                                    onFinishRating={(rating) => console.log('Rating:', rating)}
                                    style={{ backgroundColor: 'black' }}
                                />
                                <Text style={styles.reviewText}>5 | </Text>
                                <Text style={styles.reviewText}>50 Reviews</Text>
                            </View>
                            <View style={styles.ratingContainer}>

                                <Text style={styles.reviewText}>Payment : </Text>
                                <Text style={{ color: "black" }}>Online</Text>
                            </View>
                        </View>

                    </View>

                    <View style={styles.buttonContainer}>
                        <TouchableOpacity style={{ width: 100, borderColor: '#4C7CA9', backgroundColor: 'white', height: 30, borderRadius: 5, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ color: '#4C7CA9', fontSize: 16, }}>No</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ width: 100, borderColor: '#4C7CA9', backgroundColor: '#4C7CA9', height: 30, borderRadius: 5, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{
                                fontSize: 12,
                                color: 'white'
                            }}>Yes</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            }
        </>
    );
};

const styles = StyleSheet.create({
    cardContainer: {
        borderWidth: 1,
        borderColor: '#ddd',
        borderRadius: 8,
        margin: 10,
        // padding: 10,
          backgroundColor:"white"
    },
    ratingContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: 145
        // marginLeft: 10,
    },
    reviewText: {
        // marginLeft: 5,
    },
    cardHeader: {
        height: 30,
        // justifyContent: 'center',
        alignItems: 'center',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    headerText: {
        color: 'white',
        // fontWeight: 'bold',
        padding: 5
    },
    cardBody: {
        flexDirection: 'row',
        marginTop: 10,
    },
    leftContent: {
        flex: 1,
        // backgroundColor: 'yellow',
        padding: 5
    },
    cardContent: {
        padding: 10,
    },
    title: {
        fontSize: 14,
    },
    description: {
        fontSize: 12,
        color: '#434343',
        marginTop: 5,
    },
    rightContent: {
        // backgroundColor:'red',
        alignItems: 'center',
    },
    buttonContainers: {
        marginTop: 10,
        marginLeft: 30,
        alignItems: "center",
        backgroundColor: 'white',
        borderWidth: 1,
        borderColor: '#357EBD',
        borderRadius: 5,
        width: 80,
        height: 20

    },
    buttonText: {
        fontSize: 12,
        color: '#357EBD',
    },
    image: {
        width: 150,
        height: 150,
        marginRight: 10,
    },
    checkbox: {
        marginRight: 10,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: "space-around",
        padding: 10,

    },
});

export default BookingCard;
