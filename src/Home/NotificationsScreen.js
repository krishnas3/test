

// import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native'
// import React from 'react'
// import { AngleDown, Calllog, calendar, filter, menu, profile, settings } from '../../assets/Index'
// import { Image } from 'react-native'
// import { useState } from 'react'
// import DateTimePickerModal from 'react-native-modal-datetime-picker';
// import moment from 'moment';
// // import { Drawer } from 'react-native-drawer-layout';
// const NotificationsScreen = ({ navigation }) => {
//   const [selectedDate1, setSelectedDate1] = useState(new Date());
//   const [date, setDate] = useState((moment(selectedDate1).format('YYYY-MM-DD')));
//   const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
//   // method for show date modal
//   const showDatePicker = () => {
//     setDatePickerVisibility(true);
//   };
//   // method for hide date modal
//   const hideDatePicker = () => {
//     setDatePickerVisibility(false);
//     console.log(date)

//   };
//   // method for confirm date modal
//   const handleConfirm = (date) => {
//     setDate((moment(date).format('YYYY-MM-DD')));

//     hideDatePicker();
//   };
//   const openSidebar = () => {
//     navigation.openDrawer();
//   };

//   const handleOptionAClick = () => {
//     // Handle the click for "option A" here
//     // You can navigate to a specific screen or perform any other action
//     console.log('Option A clicked!');
//   };
//   return (
//     <SafeAreaView>

//       <View
//         style={{
//           backgroundColor: 'white',
//           height: 60,
//           justifyContent: 'space-between',
//           alignItems: 'center',
//           elevation: 10,

//           borderBottomEndRadius: 10,
//           // Change this to the desired color for elevation/shadow
//           shadowOffset: { width: 0, height: 2 },
//           shadowOpacity: 0.8,
//           shadowRadius: 2,
//           flexDirection: 'row',
//         }}  >
//         <Image
//           style={{ margin: 5, width: 50, height: 50, resizeMode: 'contain' }}
//           source={profile}
//         />
//         <TouchableOpacity
//           style={{ flexDirection: 'row' }}
//           onPress={() => {
//             // Handle text press action here
//           }}
//         >
//           <Text style={{ fontSize: 18, fontWeight: '100', color: '#434343' }}>
//             Delhi, India
//           </Text>
//           <Image
//             style={{ margin: 5, width: 20, height: 15, resizeMode: 'contain', tintColor: "#434343" }}
//             source={AngleDown}
//           />
//         </TouchableOpacity>

//       </View>
//       <View
//         style={{
//           backgroundColor: "white",
//           height: 60,
//           top: 5,
//           justifyContent: 'space-between',
//           alignItems: 'center',
//           elevation: 3,
//           borderWidth: 2,
//           borderColor: 'white',
//           flexDirection: 'row',

//           // Set flexDirection to 'row' to align the image and text horizontally
//         }}
//       >
//         <View>
//           <TouchableOpacity onPress={openSidebar}>
//             <View style={{ flexDirection: 'row' }}>
//               <Image
//                 style={{ margin: 5, width: 35, height: 35, resizeMode: 'contain' }}
//                 source={menu}
//               />
//               <Text style={{ fontSize: 18, fontWeight: '100', color: '#434343', alignSelf: 'center' }} onPress={handleOptionAClick}>
//                 option A
//               </Text>
//             </View>
//           </TouchableOpacity>
//         </View>
//         <TouchableOpacity
//           style={{ flexDirection: 'row', right: 5 }}
//           onPress={showDatePicker}
//         >
//           <View style={{ justifyContent: 'center', right: 15 }}>
//             <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
//               <Image style={{ margin: 5, width: 20, height: 20, resizeMode: 'contain', tintColor: '#50AFE4' }} source={calendar} />
//             </View>
//             <Text style={{ color: '#50AFE4', fontSize: 15, }}>
//               Calendar
//             </Text>
//             <DateTimePickerModal
//               isVisible={isDatePickerVisible}
//               mode="date"
//               onConfirm={handleConfirm}
//               onCancel={hideDatePicker}
//             />
//           </View>
//           <View>
//             <View style={{ justifyContent: 'center', flexDirection: 'row' }}>
//               <Image style={{ margin: 5, width: 25, height: 20, resizeMode: 'contain', tintColor: '#50AFE4' }} source={filter} />
//             </View>
//             <Text style={{ color: '#50AFE4', fontSize: 15 }}>
//               filter
//             </Text>

//           </View>
//         </TouchableOpacity>

//       </View>
//       <View>
//         <Text>Home</Text>
//       </View>
//     </SafeAreaView>
//   )
// }

// export default NotificationsScreen


import { View, Text } from 'react-native'
import React from 'react'

const NotificationsScreen = () => {
  return (
    <View>
      <Text>NotificationsScreen</Text>
    </View>
  )
}

export default NotificationsScreen