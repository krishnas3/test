import React, { Fragment } from "react";

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { SafeAreaProvider } from "react-native-safe-area-context";
import BottomTabView from "./BottomTabView";
import BottomTabViewActive from "./BottomTabViewActive";
const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

import { Homee, settings, Calllog, optionc, optiond, calendar } from "../assets/Index";

import Loader from '../assets/Loader'
import {

  Image, Text, Alert, StatusBar, ToastAndroid
} from 'react-native';

import NotificationsScreen from "../Home/NotificationsScreen";
import HomeScreen from "../Home/Home";
import Payement from "../Home/Details";

const HomeStack = () => {
  return (

    <Stack.Navigator>
     
        <Stack.Screen
        name="Home"
        component={HomeScreen}
        options={{ headerShown: false }}
      />
     
    </Stack.Navigator>
  )
}


const Settingss = () => {
  return (
    <Stack.Navigator 
    initialRouteName="Setting"
    >
      <Stack.Screen
        name="Setting"
        component={NotificationsScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}

const Dashboard = () => {
 

  return (
    <SafeAreaProvider>

      <StatusBar
        animated={true}
        backgroundColor="#edf7f6"
        barStyle='dark-content'
        hidden={false} />
      
      <Tab.Navigator
      initialRouteName="Homee"
        screenOptions={({ route }) => ({
          tabBarActiveTintColor: "#50AFE4",
          tabBarShowLabel: false,
          tabBarInactiveTintColor: "#1d1d1d",
          tabBarStyle: {
            height: 50,
            alignItems: "center",
            alignContent: "center",
            backgroundColor: '#fff',
            marginBottom: 2
          }
        })}>
        <Tab.Screen
          name="Homee"
          component={HomeStack}
          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) =>
              focused ? (
                <BottomTabViewActive
                  icon={Homee}
                  title={"Option A"}
                />
              ) : (
                <BottomTabView icon={Homee} title={"Option A"} />
              ),
          }}
        />
        <Tab.Screen
          name="CallDetails"
          component={NotificationsScreen}
          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) =>
              focused ? (
                <BottomTabViewActive
                  icon={calendar}
                  title={"Option B"}
                />
              ) : (
                <BottomTabView icon={calendar} title={"Option B"} />
              ),
          }}
        />
        <Tab.Screen
          name="Settings"
          component={NotificationsScreen}
          options={{
            headerShown: false,
            
            tabBarIcon: ({ focused }) =>
              focused ? (
                <BottomTabViewActive
                  icon={optionc}
                  title={"Option C"}
                />
              ) : (
                <BottomTabView icon={optionc} title={"Option C"} />
              ),
          }}
        />
           <Tab.Screen
          name="Option D"
          component={NotificationsScreen}
          options={{
            headerShown: false,
            
            tabBarIcon: ({ focused }) =>
              focused ? (
                <BottomTabViewActive
                  icon={optiond}
                  title={"Option D"}
                />
              ) : (
                <BottomTabView icon={optiond}  title={"Option D"} />
              ),
          }}
        />


      </Tab.Navigator>

    </SafeAreaProvider>
  );
};

export default Dashboard;

